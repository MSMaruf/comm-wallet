module core-search

go 1.16

require (
	common v0.0.0-00010101000000-000000000000
	github.com/labstack/echo-contrib v0.11.0
	github.com/labstack/echo/v4 v4.6.0
	google.golang.org/genproto v0.0.0-20210909211513-a8c4777a87af // indirect
	google.golang.org/grpc v1.40.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

replace common => ../../common
