package proto

import (
	. "common/proto"
	"context"
	"github.com/prometheus/common/log"
	"net/http"
)

type WalletService struct {
}
type CoreService struct {
}

func (c CoreService) VerifyPin(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return NewCoreAccountClient().Client.VerifyPin(ctx, info)
}

func (c CoreService) VerifyBalance(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return NewCoreAccountClient().Client.VerifyBalance(ctx, info)
}

func (c CoreService) VerifyAccounts(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return NewCoreAccountClient().Client.VerifyAccounts(ctx, info)
}

func (c CoreService) VerifyAccount(ctx context.Context, info *AccountInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return NewCoreAccountClient().Client.VerifyAccount(ctx, info)
}

func (c CoreService) LockAccount(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return NewCoreTransferClient().Client.LockAccount(ctx, info)
}

func (c CoreService) UnlockAccount(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return NewCoreTransferClient().Client.UnlockAccount(ctx, info)
}

func (c CoreService) NotifyTransfer(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return NewCoreTransferClient().Client.NotifyTransfer(ctx, info)
}

func (c CoreService) NotifyAccount(ctx context.Context, info *AccountInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return NewCoreAccountClient().Client.NotifyAccount(ctx, info)
}

func (c CoreService) PushAuditTrail(ctx context.Context, message *Message) (*Message, error) {
	log.Errorf("%v:%v", ctx, message)
	return NewCoreAccountClient().Client.PushAuditTrail(ctx, message)
}

func (c CoreService) PushAccountAction(ctx context.Context, info *AccountInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return NewCoreAccountClient().Client.PushAccountAction(ctx, info)
}

func (c CoreService) PushTransaction(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return NewCoreTransferClient().Client.PushTransaction(ctx, info)
}

func (c CoreService) PushNotification(ctx context.Context, notification *Notification) (*Message, error) {
	log.Errorf("%v:%v", ctx, notification)
	return NewCoreAccountClient().Client.PushNotification(ctx, notification)
}

func (c CoreService) mustEmbedUnimplementedCoreServiceServer() {
	panic("implement me")
}

func NewCoreService() *CoreService {
	return &CoreService{}
}
func NewWalletService() *WalletService {
	return &WalletService{}
}

func (w WalletService) GetAccount(ctx context.Context, filter *AccountFilter) (*AccountInfo, error) {
	log.Errorf("%v:%v", ctx, filter)
	return nil, http.ErrNotSupported
}
func (w WalletService) CreateAccount(ctx context.Context, filter *AccountInfo) (*AccountInfo, error) {
	log.Errorf("%v:%v", ctx, filter)
	return nil, http.ErrNotSupported
}
func (w WalletService) CloseAccount(ctx context.Context, filter *AccountInfo) (*AccountInfo, error) {
	log.Errorf("%v:%v", ctx, filter)
	return nil, http.ErrNotSupported
}

func (w WalletService) CheckAccount(ctx context.Context, filter *AccountFilter) (*Message, error) {
	log.Errorf("%v:%v", ctx, filter)
	return nil, http.ErrNotSupported
}

func (w WalletService) GetAccountBalance(ctx context.Context, filter *AccountFilter) (*BalanceInfo, error) {
	log.Errorf("%v:%v", ctx, filter)
	return nil, http.ErrNotSupported
}

func (w WalletService) GetTransaction(ctx context.Context, filter *TransactionFilter) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, filter)
	//todo: need to calling step for account service
	return nil, http.ErrNotSupported
}

func (w WalletService) FindTransactions(filter *TransactionFilter, server WalletService_FindTransactionsServer) error {
	log.Infof("%v:%v", filter, server)
	//todo: need to calling step for account service
	return http.ErrNotSupported
}

func (w WalletService) InitiateTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Errorf("%v:%v", ctx, info)
	return nil, http.ErrNotSupported
}

func (w WalletService) ConfirmTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Errorf("%v:%v", ctx, info)
	return nil, http.ErrNotSupported
}

func (w WalletService) RevertTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Errorf("%v:%v", ctx, info)
	return nil, http.ErrNotSupported
}

func (w WalletService) RequestTransfer(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Errorf("%v:%v", ctx, info)
	return nil, http.ErrNotSupported
}

func (w WalletService) ResponseTransferRequest(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Errorf("%v:%v", ctx, info)
	return nil, http.ErrNotSupported
}

func (w WalletService) ManageAccount(ctx context.Context, info *AccountInfo) (*AccountInfo, error) {
	log.Errorf("%v:%v", ctx, info)
	return nil, http.ErrNotSupported
}

func (w WalletService) mustEmbedUnimplementedWalletServiceServer() {
	panic("implement me")
}
