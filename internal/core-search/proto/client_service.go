package proto

import (
	"context"
	. "core-search/config"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
)

type CoreSearchClient struct {
	Client CoreServiceClient
	Conn   *grpc.ClientConn
}

func NewCoreSearchClient() *CoreSearchClient {
	fmt.Println("Established connection")
	conn, err := grpc.DialContext(context.Background(),
		HostConfig.RemoteSearchHost,
		grpc.WithReturnConnectionError(),
		grpc.WithBlock(),
		grpc.WithNoProxy(),
		grpc.WithInsecure())
	if err != nil || conn.GetState() == connectivity.Idle {
		fmt.Println("Failed to connect ", err)
		panic(fmt.Sprintf("Failed to connect %v", err))
	}
	fmt.Println("Get new Client")
	client := NewCoreServiceClient(conn)
	fmt.Println(client, err)
	return &CoreSearchClient{Client: client, Conn: conn}
}

type CoreAccountClient struct {
	Client CoreServiceClient
	Conn   *grpc.ClientConn
}

func NewCoreAccountClient() *CoreAccountClient {
	//panic("circular calling...")
	fmt.Println("Established connection")
	conn, err := grpc.DialContext(context.Background(),
		HostConfig.RemoteAccountHost,
		grpc.WithReturnConnectionError(),
		grpc.WithBlock(),
		grpc.WithNoProxy(),
		grpc.WithInsecure())
	if err != nil || conn.GetState() == connectivity.Idle {
		fmt.Println("Failed to connect ", err)
		panic(fmt.Sprintf("Failed to connect %v", err))
	}
	fmt.Println("Get new Client")
	client := NewCoreServiceClient(conn)
	fmt.Println(client, err)
	return &CoreAccountClient{Client: client, Conn: conn}
}

type CoreTransferClient struct {
	Client CoreServiceClient
	Conn   *grpc.ClientConn
}

func NewCoreTransferClient() *CoreTransferClient {
	fmt.Println("Established connection")
	conn, err := grpc.DialContext(context.Background(),
		HostConfig.RemoteTransferHost,
		grpc.WithReturnConnectionError(),
		grpc.WithBlock(),
		grpc.WithNoProxy(),
		grpc.WithInsecure())
	if err != nil || conn.GetState() == connectivity.Idle {
		fmt.Println("Failed to connect ", err)
		panic(fmt.Sprintf("Failed to connect %v", err))
	}
	fmt.Println("Get new Client")
	client := NewCoreServiceClient(conn)
	fmt.Println(client, err)
	return &CoreTransferClient{Client: client, Conn: conn}
}

type CoreMessagingClient struct {
	Client CoreServiceClient
	Conn   *grpc.ClientConn
}

func NewCoreMessagingClient() *CoreMessagingClient {
	fmt.Println("Established connection")
	conn, err := grpc.DialContext(context.Background(),
		HostConfig.RemoteMessagingHost,
		grpc.WithReturnConnectionError(),
		grpc.WithBlock(),
		grpc.WithNoProxy(),
		grpc.WithInsecure())
	if err != nil || conn.GetState() == connectivity.Idle {
		fmt.Println("Failed to connect ", err)
		panic(fmt.Sprintf("Failed to connect %v", err))
	}
	fmt.Println("Get new Client")
	client := NewCoreServiceClient(conn)
	fmt.Println(client, err)
	return &CoreMessagingClient{Client: client, Conn: conn}
}
