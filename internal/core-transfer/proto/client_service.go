package proto

import (
	"context"
	. "core-transfer/config"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/lovoo/goka"
	"github.com/lovoo/goka/codec"
	"github.com/prometheus/common/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"strings"
	"time"
)

type CoreSearchClient struct {
	Client CoreServiceClient
	Conn   *grpc.ClientConn
}

func PushStream(topic string, key string, msg interface{}) error {
	emitter, err := goka.NewEmitter(strings.Split(HostConfig.KafkaHosts, " "),
		goka.Stream(topic),
		new(codec.String))
	defer emitter.Finish()
	if err != nil {
		log.Fatalf("error creating emitter: %v", err)
		return err
	}
	//for {
	time.Sleep(1 * time.Second)
	err = emitter.EmitSync(key, msg)
	if err != nil {
		log.Fatalf("error emitting message: %v", err)
		return err
	}
	return nil
}

type CoreRedisClient struct {
	client *redis.Client
}

func NewCoreRedisClient() *CoreRedisClient {
	return &CoreRedisClient{client: RedisClient}
}

func (cr *CoreRedisClient) setValue(key string, val interface{}) {
	cr.client.SetNX(context.Background(), key, val, HostConfig.RedisTimeoutSecond*time.Second)
}

func (cr *CoreRedisClient) removeValue(key string) {
	cr.client.Del(context.Background(), key)
}

func (cr *CoreRedisClient) getValue(key string) interface{} {
	return cr.client.Get(context.Background(), key)
}

func NewCoreSearchClient() *CoreSearchClient {
	fmt.Println("Established connection")
	conn, err := grpc.DialContext(context.Background(),
		HostConfig.RemoteSearchHost,
		grpc.WithReturnConnectionError(),
		grpc.WithBlock(),
		grpc.WithNoProxy(),
		grpc.WithInsecure())
	if err != nil || conn.GetState() == connectivity.Idle {
		fmt.Println("Failed to connect ", err)
		panic(fmt.Sprintf("Failed to connect %v", err))
	}
	fmt.Println("Get new Client")
	client := NewCoreServiceClient(conn)
	fmt.Println(client, err)
	return &CoreSearchClient{Client: client, Conn: conn}
}

type CoreAccountClient struct {
	Client CoreServiceClient
	Conn   *grpc.ClientConn
}

func NewCoreAccountClient() *CoreAccountClient {
	//panic("circular calling...")
	fmt.Println("Established connection")
	conn, err := grpc.DialContext(context.Background(),
		HostConfig.RemoteAccountHost,
		grpc.WithReturnConnectionError(),
		grpc.WithBlock(),
		grpc.WithNoProxy(),
		grpc.WithInsecure())
	if err != nil || conn.GetState() == connectivity.Idle {
		fmt.Println("Failed to connect ", err)
		panic(fmt.Sprintf("Failed to connect %v", err))
	}
	fmt.Println("Get new Client")
	client := NewCoreServiceClient(conn)
	fmt.Println(client, err)
	return &CoreAccountClient{Client: client, Conn: conn}
}

type CoreTransferClient struct {
	Client CoreServiceClient
	Conn   *grpc.ClientConn
}

func NewCoreTransferClient() *CoreTransferClient {
	fmt.Println("Established connection")
	conn, err := grpc.DialContext(context.Background(),
		HostConfig.RemoteTransferHost,
		grpc.WithReturnConnectionError(),
		grpc.WithBlock(),
		grpc.WithNoProxy(),
		grpc.WithInsecure())
	if err != nil || conn.GetState() == connectivity.Idle {
		fmt.Println("Failed to connect ", err)
		panic(fmt.Sprintf("Failed to connect %v", err))
	}
	fmt.Println("Get new Client")
	client := NewCoreServiceClient(conn)
	fmt.Println(client, err)
	return &CoreTransferClient{Client: client, Conn: conn}
}

type CoreMessagingClient struct {
	Client CoreServiceClient
	Conn   *grpc.ClientConn
}

func NewCoreMessagingClient() *CoreMessagingClient {
	fmt.Println("Established connection")
	conn, err := grpc.DialContext(context.Background(),
		HostConfig.RemoteMessagingHost,
		grpc.WithReturnConnectionError(),
		grpc.WithBlock(),
		grpc.WithNoProxy(),
		grpc.WithInsecure())
	if err != nil || conn.GetState() == connectivity.Idle {
		fmt.Println("Failed to connect ", err)
		panic(fmt.Sprintf("Failed to connect %v", err))
	}
	fmt.Println("Get new Client")
	client := NewCoreServiceClient(conn)
	fmt.Println(client, err)
	return &CoreMessagingClient{Client: client, Conn: conn}
}
