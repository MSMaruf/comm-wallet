package proto

import (
	. "common/proto"
	"context"
	"core-transfer/config"
	. "core-transfer/schema"
	"fmt"
	"github.com/google/uuid"
	"github.com/prometheus/common/log"
	"net/http"
)

type WalletService struct {
}
type CoreService struct{}

func (c CoreService) VerifyPin(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	return NewCoreAccountClient().Client.VerifyPin(ctx, info)
}

func (c CoreService) VerifyBalance(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	return NewCoreAccountClient().Client.VerifyBalance(ctx, info)
}

func (c CoreService) VerifyAccounts(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	return NewCoreAccountClient().Client.VerifyAccounts(ctx, info)
}

func (c CoreService) VerifyAccount(ctx context.Context, info *AccountInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	return NewCoreAccountClient().Client.VerifyAccount(ctx, info)
}

func (c CoreService) LockAccount(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	return lockAccount(info)
}

func lockAccount(info *TransactionInfo) (*Message, error) {
	log.Infof("Locking account-balance: %v", info)
	rc := NewCoreRedisClient()
	value := rc.getValue(fmt.Sprintf("Lock-Balance:%v:%v", info.SourceId, info.Source))
	if value != nil {
		log.Errorf("Already Locked: %v", info)
		return &Message{
			Msg:   fmt.Sprintf("Account Already Locked: %v: %v", info.SourceId, info.Source),
			Valid: false,
			Err:   fmt.Sprintf("%v", http.ErrNotSupported),
		}, http.ErrNotSupported
	}
	rc.setValue(fmt.Sprintf("Lock-Balance:%v:%v", info.SourceId, info.Source), info)
	return &Message{
		Msg:   fmt.Sprintf("Account Locked: %v: %v", info.SourceId, info.Source),
		Valid: true,
	}, nil
}

func (c CoreService) UnlockAccount(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	return unlockAccount(info)
}

func unlockAccount(info *TransactionInfo) (*Message, error) {
	log.Infof("Locking account-balance: %v", info)
	rc := NewCoreRedisClient()
	value := rc.getValue(fmt.Sprintf("Lock-Balance:%v:%v", info.SourceId, info.Source))
	if value != nil {
		log.Errorf("Already unlocked: %v", info)
		return &Message{
			Msg:   fmt.Sprintf("Account Already Unlocked: %v: %v", info.SourceId, info.Source),
			Valid: false,
			Err:   fmt.Sprintf("%v", http.ErrNotSupported),
		}, http.ErrNotSupported
	}
	rc.removeValue(fmt.Sprintf("Lock-Balance:%v:%v", info.SourceId, info.Source))
	return &Message{
		Msg:   fmt.Sprintf("Account Locked: %v: %v", info.SourceId, info.Source),
		Valid: true,
	}, nil
}

func cacheTransfer(info *TransactionInfo) error {
	log.Infof("Cache working transfer: %v", info)
	rc := NewCoreRedisClient()
	value := rc.getValue(fmt.Sprintf("Lock-Balance:%v:%v", info.SourceId, info.Source))
	if value != nil {
		log.Errorf("Invalid Transfer: %v", info)
		return http.ErrNotSupported
	}
	rc.setValue(fmt.Sprintf("Cache-Transfer:%v:%v:%v:%v:%v", info.SourceId, info.Source, info.TargetId,
		info.Target, info.Amount), info)
	return nil
}

func unCacheTransfer(info *TransactionInfo) error {
	log.Infof("Cache working transfer: %v", info)
	NewCoreRedisClient().removeValue(fmt.Sprintf("Cache-Transfer:%v:%v:%v:%v:%v", info.SourceId, info.Source,
		info.TargetId, info.Target, info.Amount))
	return nil
}

func (c CoreService) NotifyTransfer(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	return pushTransaction(info)
}

func (c CoreService) NotifyAccount(ctx context.Context, info *AccountInfo) (*Message, error) {
	log.Errorf("%v:%v:%v", ctx, info, http.ErrNotSupported)
	return NewCoreAccountClient().Client.NotifyAccount(ctx, info)
}

func (c CoreService) PushAuditTrail(ctx context.Context, message *Message) (*Message, error) {
	log.Errorf("%v:%v", ctx, message)
	return NewCoreAccountClient().Client.PushAuditTrail(ctx, message)
}

func (c CoreService) PushAccountAction(ctx context.Context, info *AccountInfo) (*Message, error) {
	log.Errorf("%v:%v:%v", ctx, info, http.ErrNotSupported)
	return NewCoreAccountClient().Client.PushAccountAction(ctx, info)
}

func (c CoreService) PushTransaction(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	return pushTransaction(info)
}

func pushTransaction(info *TransactionInfo) (*Message, error) {
	log.Infof("Pushed Transfer as Kafka: %v", info)
	uid, _ := uuid.NewUUID()
	err := PushStream(config.HostConfig.KafkaTopicTransferStatus, uid.URN(), info)
	if err != nil {
		log.Errorf("Pushed Failed: %v", err)
		return &Message{
			Msg:   fmt.Sprintf("Pushed Failed: %v", err),
			Valid: false,
			Err:   fmt.Sprintf("%v", err),
		}, err
	}
	return &Message{
		Msg:   fmt.Sprintf("Pushed: %v", info),
		Valid: true,
	}, nil
}

func (c CoreService) PushNotification(ctx context.Context, notification *Notification) (*Message, error) {
	log.Errorf("%v:%v", ctx, notification)
	return NewCoreAccountClient().Client.PushNotification(ctx, notification)

}

func (c CoreService) mustEmbedUnimplementedCoreServiceServer() {
	panic("implement me")
}

func NewCoreService() *CoreService {
	return &CoreService{}
}

func NewWalletService() *WalletService {
	return &WalletService{}
}

func (w WalletService) GetAccount(ctx context.Context, filter *AccountFilter) (*AccountInfo, error) {
	log.Errorf("%v:%v", ctx, filter)
	return &AccountInfo{
		Msg: "Need to call account-service",
		Err: fmt.Sprintf("%v", http.ErrNotSupported),
	}, http.ErrNotSupported
}
func (w WalletService) CreateAccount(ctx context.Context, filter *AccountInfo) (*AccountInfo, error) {
	log.Errorf("%v:%v", ctx, filter)
	return &AccountInfo{
		Msg: "Need to call account-service",
		Err: fmt.Sprintf("%v", http.ErrNotSupported),
	}, http.ErrNotSupported
}
func (w WalletService) CloseAccount(ctx context.Context, filter *AccountInfo) (*AccountInfo, error) {
	log.Errorf("%v:%v", ctx, filter)
	return &AccountInfo{
		Msg: "Need to call account-service",
		Err: fmt.Sprintf("%v", http.ErrNotSupported),
	}, http.ErrNotSupported
}

func (w WalletService) CheckAccount(ctx context.Context, filter *AccountFilter) (*Message, error) {
	log.Errorf("%v:%v", ctx, filter)
	return &Message{
		Msg: "Need to call account-service",
		Err: fmt.Sprintf("%v", http.ErrNotSupported),
	}, http.ErrNotSupported
}

func (w WalletService) GetAccountBalance(ctx context.Context, filter *AccountFilter) (*BalanceInfo, error) {
	log.Errorf("%v:%v", ctx, filter)
	return &BalanceInfo{
		Msg:   "Need to call account-service",
		Error: fmt.Sprintf("%v", http.ErrNotSupported),
	}, http.ErrNotSupported
}

func (w WalletService) GetTransaction(ctx context.Context, filter *TransactionFilter) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, filter)
	//todo: need to calling step for account service
	return nil, http.ErrNotSupported
}

func (w WalletService) FindTransactions(filter *TransactionFilter, server WalletService_FindTransactionsServer) error {
	log.Errorf("%v:%v", filter, server)
	return http.ErrNotSupported
}

func (w WalletService) InitiateTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, info)
	cc := NewCoreAccountClient()
	_, err := cc.Client.VerifyPin(ctx, info)
	if err != nil {
		log.Errorf("Pin failed: %v", info)
		info.Err = fmt.Sprintf("%v", err)
		info.Msg = "Pin failed"
		return info, err
	}
	_, err2 := cc.Client.VerifyAccounts(ctx, info)
	if err2 != nil {
		log.Errorf("Account Not Verified: %v", info)
		info.Err = fmt.Sprintf("%v", err2)
		info.Msg = "Account Not Verified"
		return info, err2
	}
	_, err3 := lockAccount(info)
	_, err31 := cc.Client.VerifyBalance(ctx, info)
	if err31 != nil {
		log.Errorf("Balance changed: %v", err31)
		info.Err = fmt.Sprintf("%v", err3)
		info.Msg = "Account Balance Changed"
		_, _ = unlockAccount(info)
		return info, err31
	}
	if err3 != nil {
		log.Errorf("Account Locking failed: %v", info)
		info.Err = fmt.Sprintf("%v", err3)
		info.Msg = "Account Locking failed"
		return info, err3
	}
	trf, err4 := initiateTransfer(info)
	if err4 != nil {
		log.Errorf("Transfer failed: %v", info)
		info.Err = fmt.Sprintf("%v", err4)
		info.Msg = "Transfer failed"
		_, _ = unlockAccount(info)
		return info, err4
	}
	trf.Msg = "Transaction Initiated"
	err5 := cacheTransfer(info)
	if err5 != nil {
		info.Err = fmt.Sprintf("%v", err5)
		info.Msg = "Transfer failed"
		_, _ = unlockAccount(info)
		return info, err5
	}
	return trf, nil
}

func initiateTransfer(info *TransactionInfo) (*TransactionInfo, error) {
	log.Infof("Storing Requested transfer: %v", info)
	info.Status = TransactionStatus_Requested
	info.Type = TransactionType_Account
	return NewTransferDao().InitiateTransfer(info)
}

func (w WalletService) ConfirmTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, info)
	err := checkTransferInitiation(info)
	if err != nil {
		log.Errorf("Failed to complete: %v", err)
		info.Msg = "Failed to Complete"
		info.Err = fmt.Sprintf("%v", err)
		_, _ = unlockAccount(info)
		return info, http.ErrNotSupported
	}
	trf, err2 := processTransfer(info)
	if err2 != nil {
		log.Errorf("Transfer Failed to Complete: %v", err2)
		info.Msg = "Failed to complete"
		info.Err = fmt.Sprintf("%v", err2)
		_, _ = unlockAccount(info)
		return info, err2
	}
	_, _ = unlockAccount(trf)
	_ = unCacheTransfer(trf)
	log.Debugf("Successful Transfer: %v", info)
	go func() {
		_, _ = pushTransaction(trf)
	}()
	return trf, nil
}

func processTransfer(info *TransactionInfo) (*TransactionInfo, error) {
	log.Infof("Store Transfer in Journal and Ledger: %v", info)
	info.Type = TransactionType_Account
	info.Status = TransactionStatus_Confirmed
	return NewTransferDao().ConfirmTransfer(info)
}

func checkTransferInitiation(info *TransactionInfo) error {
	log.Infof("Checking transfer initiation: %v", info)
	rc := NewCoreRedisClient()
	value := rc.getValue(fmt.Sprintf("Lock-Balance:%v:%v", info.SourceId, info.Source))
	if value == nil {
		log.Errorf("Session not exist: %", http.ErrNotSupported)
		return http.ErrNotSupported
	}
	value2 := rc.getValue(fmt.Sprintf("Cache-Transfer:%v:%v:%v:%v:%v", info.SourceId, info.Source, info.TargetId,
		info.Target, info.Amount))
	if value2 == nil {
		log.Errorf("Transfer not initiated or timeout: %v", info)
		return http.ErrNotSupported
	}
	return nil
}

func (w WalletService) RevertTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, info)
	//todo: need to calling step for account service
	return nil, http.ErrNotSupported
}

func (w WalletService) RequestTransfer(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	//todo: need to calling step for account service
	return nil, http.ErrNotSupported
}

func (w WalletService) ResponseTransferRequest(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, info)
	//todo: need to calling step for account service
	return nil, http.ErrNotSupported
}

func (w WalletService) ManageAccount(ctx context.Context, info *AccountInfo) (*AccountInfo, error) {
	log.Errorf("%v:%v:%v", ctx, info, http.ErrNotSupported)
	info.Msg = "Not Supported"
	info.Err = fmt.Sprintf("%v", http.ErrNotSupported)
	return info, http.ErrNotSupported
}

func (w WalletService) mustEmbedUnimplementedWalletServiceServer() {
	panic("implement me")
}
