module core-transfer

go 1.16

require (
	common v0.0.0-00010101000000-000000000000
	github.com/Shopify/sarama v1.30.0 // indirect
	github.com/go-pg/pg/v10 v10.10.6
	github.com/go-redis/redis/v8 v8.11.4
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/golang/mock v1.6.0 // indirect
	github.com/google/uuid v1.1.2
	github.com/labstack/echo-contrib v0.11.0
	github.com/labstack/echo/v4 v4.6.0
	github.com/lovoo/goka v1.0.6
	github.com/prometheus/common v0.25.0
	golang.org/x/net v0.0.0-20211008194852-3b03d305991f // indirect
	google.golang.org/genproto v0.0.0-20210909211513-a8c4777a87af // indirect
	google.golang.org/grpc v1.40.0
	gopkg.in/yaml.v2 v2.4.0
)

replace common => ../../common
