package model

import (
	"common/proto"
	"context"
	. "core-transfer/config"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
	"github.com/prometheus/common/log"
	"net/http"
	"strconv"
)

type TransferDao struct {
	Db *pg.DB
}

func NewTransferDao() *TransferDao {
	return &TransferDao{}
}

func (ac *TransferDao) initiateConnection() *pg.DB {
	return pg.Connect(&pg.Options{
		User:     PgConfig.User,
		Password: PgConfig.Password,
		Database: PgConfig.Database,
		Addr:     PgConfig.Host,
		OnConnect: func(ctx context.Context, cn *pg.Conn) error {
			_, err := cn.Exec("set search_path=?", PgConfig.Schema)
			if err != nil {
				return err
			}
			return nil
		},
	})
}

func (ac *TransferDao) InitiateTransfer(info *common.TransactionInfo) (*common.TransactionInfo, error) {
	log.Infof("Start Action for DB: %v", info)
	ac.Db = ac.initiateConnection()
	defer ac.Db.Close()
	uid, _ := uuid.NewUUID()
	previous, _ := strconv.ParseFloat(info.Previous, 32)
	balance, _ := strconv.ParseFloat(info.Balance, 32)
	amount, _ := strconv.ParseFloat(info.Amount, 32)
	sourceId, _ := strconv.ParseInt(info.SourceId, 10, 32)
	targetId, _ := strconv.ParseInt(info.TargetId, 10, 32)
	journal := Journal{
		IdHash:      uid.URN(),
		UuidTrx:     uid,
		AccSource:   info.Source,
		AccTarget:   info.Target,
		AccSrcId:    int32(sourceId),
		AccTrgId:    int32(targetId),
		PrevBalance: float32(previous),
		NextBalance: float32(balance),
		TrxType:     int32(info.Status),
		TrxStatus:   int32(info.Status),
		TrxChannel:  "UPAY",
		Remarks:     "Transfer Initiated",
		Amount:      float32(amount),
	}
	result, err := ac.Db.Model(&journal).Insert()
	if err != nil {
		log.Errorf("Transfer failed: %v", err)
		return info, err
	} else if result.RowsAffected() == 0 {
		log.Errorf("Transfer failed: No row effected: %v", http.ErrNotSupported)
	}
	log.Debugf("Successfully Initiated: %v", info)
	info.Id = fmt.Sprintf("%v", journal.Id)
	return info, nil
}

func (ac *TransferDao) ConfirmTransfer(info *common.TransactionInfo) (*common.TransactionInfo, error) {
	log.Infof("Confirming Transfer: %v", info)
	ac.Db = ac.initiateConnection()
	defer ac.Db.Close()
	err := ac.Db.RunInTransaction(context.Background(),
		func(tx *pg.Tx) error {
			amount, _ := strconv.ParseFloat(info.Amount, 32)
			sourceId, _ := strconv.ParseInt(info.SourceId, 10, 32)
			targetId, _ := strconv.ParseInt(info.TargetId, 10, 32)
			id, _ := strconv.ParseInt(info.Id, 10, 64)
			journal, err6, done := ac.updateJournal(id, sourceId, targetId, info, amount)
			if done {
				return err6
			}
			suid, _ := uuid.NewUUID()
			tsuid, _ := uuid.NewUUID()
			ledger, err, done2 := ac.updateSourceLedger(journal, suid)
			if done2 {
				return err
			}
			target, err2, done3 := ac.updateTargetLedger(journal, ledger, tsuid)
			if done3 {
				return err2
			}
			log.Debugf("Transfer successfully completed: %v:%v:%v", journal, ledger, target)
			return nil
		})
	if err != nil {
		log.Errorf("Failed Transfer: %v", err)
		info.Msg = "Failed Confirm"
		info.Err = fmt.Sprintf("%v", err)
		return info, err
	}
	return info, nil
}

func (ac *TransferDao) updateTargetLedger(journal Journal, ledger Ledger, tsuid uuid.UUID) (Ledger, error, bool) {
	log.Infof("Update target ledger: %v:%v", journal.Id, journal.AccTrgId)
	ledgerLast := Ledger{
		AccNo: journal.AccTarget,
		AccId: journal.AccTrgId,
	}
	err4 := ac.Db.Model(&ledger).Last()
	if err4 != nil {
		log.Errorf("Failed to select target ledger: %v", err4)
		return ledgerLast, err4, true
	} else if ledgerLast.Id == 0 {
		ledgerLast.Balance = 0.0
	}

	ledgerTarget := Ledger{
		Amount:     journal.Amount,
		Balance:    ledgerLast.Balance + journal.Amount,
		Debit:      1,
		HashId:     tsuid.URN(),
		TrxChannel: journal.TrxChannel,
		TrxId:      int32(journal.Id),
		TrxUid:     journal.UuidTrx,
		TrxHashId:  journal.IdHash,
		AccId:      journal.AccTrgId,
		AccNo:      journal.AccTarget,
		OriginDesc: journal.AccSource,
		OriginId:   journal.AccSrcId,
		TrxType:    journal.TrxType,
		Remarks:    journal.Remarks,
	}
	insert2, err5 := ac.Db.Model(ledgerTarget).Insert()
	if err5 != nil {
		log.Errorf("Failed to update target ledger: %v", err5)
		return ledgerTarget, err5, true
	} else if insert2.RowsAffected() == 0 {
		log.Errorf("Failed to effect target ledger: %v", http.ErrNotSupported)
		return ledgerTarget, http.ErrNotSupported, true
	}
	return ledgerTarget, nil, false
}

func (ac *TransferDao) updateSourceLedger(journal Journal, suid uuid.UUID) (Ledger, error, bool) {
	ledger := Ledger{
		Amount:     journal.Amount,
		Balance:    journal.NextBalance,
		Debit:      0,
		HashId:     suid.URN(),
		TrxChannel: journal.TrxChannel,
		TrxId:      int32(journal.Id),
		TrxUid:     journal.UuidTrx,
		TrxHashId:  journal.IdHash,
		AccId:      journal.AccSrcId,
		AccNo:      journal.AccSource,
		OriginDesc: journal.AccTarget,
		OriginId:   journal.AccTrgId,
		TrxType:    journal.TrxType,
		Remarks:    journal.Remarks,
	}
	insert, err3 := ac.Db.Model(&ledger).Insert()
	if err3 != nil {
		log.Errorf("Failed to update source ledger: %v", err3)
		return Ledger{}, err3, true
	} else if insert.RowsAffected() == 0 {
		log.Errorf("Failed to effect source ledger: %v", http.ErrNotSupported)
		return Ledger{}, http.ErrNotSupported, true
	}
	return ledger, nil, false
}

func (ac *TransferDao) updateJournal(id int64, sourceId int64, targetId int64, info *common.TransactionInfo,
	amount float64) (Journal, error, bool) {
	log.Infof("Update Journal: %v-%v", id, sourceId)
	journal := Journal{
		Id:        id,
		AccSrcId:  int32(sourceId),
		AccTrgId:  int32(targetId),
		AccSource: info.Source,
		AccTarget: info.Target,
		Amount:    float32(amount),
	}
	err := ac.Db.Model(&journal).Select()
	if err != nil {
		log.Errorf("Failed to found: %v", err)
		return Journal{}, err, true
	}
	journal.TrxStatus = int32(info.Status)
	journal.Remarks = "Confirmed"
	result, err2 := ac.Db.Model(&journal).Update()
	if err2 != nil {
		log.Errorf("Failed to update: %v", err2)
		return Journal{}, err2, true
	} else if result.RowsAffected() == 0 {
		log.Errorf("No row effected: %v", http.ErrNotSupported)
		return Journal{}, http.ErrNotSupported, true
	}
	log.Debugf("Journal Updated: %v", journal)
	return journal, nil, false
}
