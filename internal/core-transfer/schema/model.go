package model

import (
	"github.com/google/uuid"
	"time"
)

type Account struct {
	TableName      struct{}  `sql:"cws.account,alias:ac" pg:",discard_unknown_columns"`
	AccName        string    `gorm:"type:VARCHAR;NOT NULL"`
	AccNo          string    `gorm:"type:VARCHAR;NOT NULL"`
	InitialBalance float32   `gorm:"type:NUMERIC;NOT NULL"`
	LastBalance    float32   `gorm:"type:NUMERIC;NOT NULL"`
	AccStatus      int32     `gorm:"type:INT8;NOT NULL"`
	AccType        int32     `gorm:"type:INT8;NOT NULL"`
	Created        time.Time `gorm:"type:TIMESTAMP;NOT NULL"`
	Updated        time.Time `gorm:"type:TIMESTAMP;"`
	Closed         time.Time `gorm:"type:TIMESTAMP;"`
	Remarks        string    `gorm:"type:VARCHAR;"`
	EncPin         string    `gorm:"type:VARCHAR;NOT NULL"`
	HashId         string    `gorm:"type:VARCHAR;NOT NULL"`
	Id             int64     `gorm:"type:SERIAL;PRIMARY_KEY;NOT NULL"`
}
type Journal struct {
	TableName   struct{}  `sql:"cws.journal,alias:ac" pg:",discard_unknown_columns"`
	Id          int64     `gorm:"type:SERIAL;PRIMARY_KEY;NOT NULL"`
	IdHash      string    `gorm:"type:VARCHAR;NOT NULL"`
	AccSource   string    `gorm:"type:VARCHAR;NOT NULL"`
	AccTarget   string    `gorm:"type:VARCHAR;NOT NULL"`
	PrevBalance float32   `gorm:"type:NUMERIC;NOT NULL"`
	NextBalance float32   `gorm:"type:NUMERIC;NOT NULL"`
	Amount      float32   `gorm:"type:NUMERIC;NOT NULL"`
	AccSrcId    int32     `gorm:"type:INT4;NOT NULL"`
	AccTrgId    int32     `gorm:"type:INT4;NOT NULL"`
	UuidTrx     uuid.UUID `gorm:"type:UUID;NOT NULL"`
	Created     time.Time `gorm:"type:TIMESTAMP;NOT NULL"`
	Updated     time.Time `gorm:"type:TIMESTAMP;NOT NULL"`
	TrxStatus   int32     `gorm:"type:INT8;NOT NULL"`
	TrxType     int32     `gorm:"type:INT8;NOT NULL"`
	TrxChannel  string    `gorm:"type:VARCHAR;NOT NULL"`
	Remarks     string    `gorm:"type:VARCHAR;NOT NULL"`
}
type Ledger struct {
	TableName  struct{}  `sql:"cws.ledger,alias:ac" pg:",discard_unknown_columns"`
	Id         int64     `gorm:"type:SERIAL;PRIMARY_KEY;NOT NULL"`
	HashId     string    `gorm:"type:VARCHAR;NOT NULL"`
	AccNo      string    `gorm:"type:VARCHAR;NOT NULL"`
	AccId      int32     `gorm:"type:INT4;NOT NULL"`
	TrxId      int32     `gorm:"type:INT4;NOT NULL"`
	TrxHashId  string    `gorm:"type:VARCHAR;NOT NULL"`
	TrxUid     uuid.UUID `gorm:"type:UUID;NOT NULL"`
	Amount     float32   `gorm:"type:NUMERIC;NOT NULL"`
	Balance    float32   `gorm:"type:NUMERIC;NOT NULL"`
	Created    time.Time `gorm:"type:TIMESTAMP;NOT NULL"`
	Debit      int32     `gorm:"type:INT2;NOT NULL"`
	TrxChannel string    `gorm:"type:VARCHAR;NOT NULL"`
	Remarks    string    `gorm:"type:VARCHAR;NOT NULL"`
	OriginId   int32     `gorm:"type:INT4;NOT NULL"`
	OriginDesc string    `gorm:"type:VARCHAR;NOT NULL"`
	TrxType    int32     `gorm:"type:INT8;NOT NULL"`
}
