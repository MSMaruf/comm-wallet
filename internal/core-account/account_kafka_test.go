package main

import (
	"github.com/stretchr/testify/assert"
	"log"
	"net/http"
	"testing"
	"time"

	"github.com/lovoo/goka"
	"github.com/lovoo/goka/codec"
)

var (
	brokers             = []string{"host:9092"}
	topic   goka.Stream = "example-stream"
	group   goka.Group  = "example-group"
)

func Test_KafkaProducer(t *testing.T) {
	emitter, err := goka.NewEmitter(brokers, topic, new(codec.String))
	if err != nil {
		log.Fatalf("error creating emitter: %v", err)
	}
	defer emitter.Finish()
	//for {
	time.Sleep(1 * time.Second)
	err = emitter.EmitSync("some-key", "some-value")
	if err != nil {
		log.Fatalf("error emitting message: %v", err)
	}
	//}
	assert.Equal(t, http.StatusOK, 200)
}
