package proto

import (
	. "common/proto"
	"context"
	config "core-account/config"
	model "core-account/schema"
	"fmt"
	"github.com/google/uuid"
	"github.com/prometheus/common/log"
	"net/http"
)

type WalletService struct {
}

type CoreService struct{}

func (c CoreService) VerifyPin(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	account, err := model.NewAccountDao().GetAccount(info.SourceId, "", AccountType_Wallet,
		info.Source, AccountStatus_Active)
	if err != nil {
		log.Errorf("Error: %v", err)
		return &Message{
			Err:   fmt.Sprintf("%v", err),
			Msg:   "Error",
			Valid: false,
		}, err
	} else if account.EncPin != info.Pin {
		log.Errorf("Error: Failed %v", http.ErrNotSupported)
		return &Message{
			Err:   fmt.Sprintf("%v", http.ErrNotSupported),
			Msg:   "Error",
			Valid: false,
		}, http.ErrNotSupported
	}
	log.Debugf("Verified: %v", info)
	return &Message{
		Msg:   "Verified",
		Valid: true,
		Exist: true,
	}, nil
}

func (c CoreService) VerifyBalance(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	balance, err := model.NewAccountDao().GetAccountBalance(info.SourceId, AccountType_Wallet,
		AccountStatus_Active)
	if err != nil {
		log.Errorf("Error: %v", err)
		return &Message{
			Err:   fmt.Sprintf("%v", err),
			Msg:   "Balance Not Exist",
			Exist: false,
			Valid: false,
		}, err
	} else if balance.Balance != info.Previous {
		log.Errorf("Failed: %v", http.ErrNotSupported)
		return &Message{
			Err:   fmt.Sprintf("%v", http.ErrNotSupported),
			Msg:   "Balance Already Changed",
			Exist: false,
			Valid: false,
		}, http.ErrNotSupported
	} else if balance.Balance < info.Amount {
		log.Errorf("Error insufficient: %v", http.ErrNotSupported)
		return &Message{
			Err:   fmt.Sprintf("%v", http.ErrNotSupported),
			Msg:   "Balance Insufficient",
			Exist: false,
			Valid: false,
		}, http.ErrNotSupported
	}
	log.Debugf("Possible: %v of %v", info.Amount, balance.Balance)
	return &Message{
		Msg:   "Balance Sufficient",
		Exist: true,
		Valid: true,
	}, nil
}

func (c CoreService) VerifyAccounts(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	exist, err := model.NewAccountDao().IsAccountExist(info.SourceId, info.Source, AccountType_Wallet,
		AccountStatus_Active)
	if err != nil {
		log.Errorf("Source Error: %v", err)
		return &Message{
			Err:   fmt.Sprintf("%v", err),
			Msg:   "Source Not Found",
			Exist: false,
			Valid: false,
		}, err
	} else if exist == false {
		log.Errorf("Source Error: %v", http.ErrNotSupported)
		return &Message{
			Err:   fmt.Sprintf("%v", http.ErrNotSupported),
			Msg:   "Source Not Found",
			Exist: true,
			Valid: true,
		}, http.ErrNotSupported
	}
	exist2, err2 := model.NewAccountDao().IsAccountExist(info.TargetId, info.Target, AccountType_Wallet,
		AccountStatus_Active)
	if err2 != nil {
		log.Errorf("Target Error: %v", err2)
		return &Message{
			Err:   fmt.Sprintf("%v", err2),
			Msg:   "Target Not Found",
			Exist: false,
			Valid: false,
		}, err2
	} else if exist2 == false {
		log.Errorf("Target Error: %v", http.ErrNotSupported)
		return &Message{
			Err:   fmt.Sprintf("%v", http.ErrNotSupported),
			Msg:   "Target Not Found",
			Exist: false,
			Valid: false,
		}, http.ErrNotSupported
	}
	log.Debugf("Verified Target and Source: %v", info)
	return &Message{
		Msg:   "Source and Target verified",
		Exist: true,
		Valid: true,
	}, nil
}

func (c CoreService) VerifyAccount(ctx context.Context, info *AccountInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	exist, err := model.NewAccountDao().IsAccountExist(info.Id, info.Number, AccountType_Wallet,
		AccountStatus_Active)
	if err != nil {
		log.Errorf("Account Error: %v", err)
		return &Message{
			Err:   fmt.Sprintf("%v", err),
			Msg:   "Source Not Found",
			Exist: false,
			Valid: false,
		}, err
	} else if exist == false {
		log.Errorf("Account Error: %v", http.ErrNotSupported)
		return &Message{
			Err:   fmt.Sprintf("%v", http.ErrNotSupported),
			Msg:   "Source Not Found",
			Exist: true,
			Valid: true,
		}, http.ErrNotSupported
	}
	log.Debugf("Verified Account: %v", info)
	return &Message{
		Msg:   "Account verified",
		Exist: true,
		Valid: true,
	}, nil
}

func (c CoreService) LockAccount(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	return NewCoreTransferClient().Client.LockAccount(ctx, info)
}

func (c CoreService) UnlockAccount(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	return NewCoreTransferClient().Client.UnlockAccount(ctx, info)
}

func (c CoreService) NotifyTransfer(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Errorf("Error: %v", http.ErrNotSupported)
	return NewCoreTransferClient().Client.NotifyTransfer(ctx, info)
}

func (c CoreService) NotifyAccount(ctx context.Context, info *AccountInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	uid, _ := uuid.NewUUID()
	err := PushStream(config.HostConfig.KafkaTopicAccountStatus, uid.URN(), info)
	if err != nil {
		log.Errorf("Pushed Failed: %v", err)
		return &Message{
			Msg:   fmt.Sprintf("Pushed: %v", err),
			Valid: false,
			Err:   fmt.Sprintf("%v", err),
		}, err
	}
	return &Message{
		Msg:   fmt.Sprintf("Pushed: %v", info),
		Valid: true,
	}, nil
}

func (c CoreService) PushAuditTrail(ctx context.Context, message *Message) (*Message, error) {
	log.Infof("%v:%v", ctx, message)
	uid, _ := uuid.NewUUID()
	err := PushStream(config.HostConfig.KafkaTopicAuditTail, uid.URN(), message)
	if err != nil {
		log.Errorf("Pushed Failed: %v", err)
		return &Message{
			Msg:   fmt.Sprintf("Pushed: %v", err),
			Valid: false,
			Err:   fmt.Sprintf("%v", err),
		}, err
	}
	return &Message{
		Msg:   fmt.Sprintf("Pushed: %v", message),
		Valid: true,
	}, nil
}

func (c CoreService) PushAccountAction(ctx context.Context, info *AccountInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	uid, _ := uuid.NewUUID()
	err := PushStream(config.HostConfig.KafkaTopicAccountStatus, uid.URN(), info)
	if err != nil {
		log.Errorf("Pushed Failed: %v", err)
		return &Message{
			Msg:   fmt.Sprintf("Pushed: %v", err),
			Valid: false,
			Err:   fmt.Sprintf("%v", err),
		}, err
	}
	return &Message{
		Msg:   fmt.Sprintf("Pushed: %v", info),
		Valid: true,
	}, nil
}

func (c CoreService) PushTransaction(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Errorf("Error: %v", http.ErrNotSupported)
	return NewCoreTransferClient().Client.PushTransaction(ctx, info)
}

func (c CoreService) PushNotification(ctx context.Context, notification *Notification) (*Message, error) {
	log.Infof("%v:%v", ctx, notification)
	uid, _ := uuid.NewUUID()
	err := PushStream(config.HostConfig.KafkaTopicNotifyStatus, uid.URN(), notification)
	if err != nil {
		log.Errorf("Pushed Failed: %v", err)
		return &Message{
			Msg:   fmt.Sprintf("Pushed: %v", err),
			Valid: false,
			Err:   fmt.Sprintf("%v", err),
		}, err
	}
	return &Message{
		Msg:   fmt.Sprintf("Pushed: %v", notification),
		Valid: true,
	}, nil
}

func (c CoreService) mustEmbedUnimplementedCoreServiceServer() {
	panic("implement me")
}

func NewCoreService() *CoreService {
	return &CoreService{}
}

func NewWalletService() *WalletService {
	return &WalletService{}
}

func (w WalletService) GetAccount(ctx context.Context, filter *AccountFilter) (*AccountInfo, error) {
	log.Infof("Get Account:...%v:%v", ctx, filter)
	account, err := model.NewAccountDao().
		GetAccount(filter.Id, filter.Name, filter.Type, filter.Number, filter.Status)
	if err != nil {
		log.Errorf("Exception: %v", err)
		return nil, err
	}
	log.Debugf("Result:...%v", account)
	return &AccountInfo{Id: fmt.Sprintf("%v", account.Id),
		Name:    account.AccName,
		Number:  account.AccNo,
		Balance: fmt.Sprintf("%v", account.LastBalance),
	}, nil
}
func (w WalletService) CreateAccount(ctx context.Context, info *AccountInfo) (*AccountInfo, error) {
	log.Infof("Creating new account:...%v:%v", ctx, info)
	exist, err := model.NewAccountDao().IsAccountExist(info.Id, info.Number, info.Type, info.Status)
	if err != nil {
		log.Errorf("Error in creating: %v", err)
		return nil, err
	} else if exist {
		log.Errorf("Error in creating: %v", http.ErrNotSupported)
		return nil, http.ErrNotSupported
	}
	created, err2 := model.NewAccountDao().CreateNewAccount(info)
	if err2 != nil {
		log.Errorf("Error in creating: %v", err2)
		return nil, err2
	}
	log.Debugf("Result: %v", created)
	return created, nil
}
func (w WalletService) CloseAccount(ctx context.Context, info *AccountInfo) (*AccountInfo, error) {
	log.Infof("Closing Account: %v:%v", ctx, info)
	exist, err := model.NewAccountDao().IsAccountExist(info.Id, info.Number, info.Type, info.Status)
	if err != nil {
		log.Errorf("Error in closing: %v", err)
		return nil, err
	} else if exist {
		log.Errorf("Error in closing: %v", http.ErrNotSupported)
		return nil, http.ErrNotSupported
	}
	closed, err2 := model.NewAccountDao().CloseAccount(info)
	if err2 != nil {
		log.Errorf("Error in creating: %v", err2)
		return nil, err2
	}
	log.Debugf("Result: %v", closed)
	return closed, nil
}

func (w WalletService) CheckAccount(ctx context.Context, info *AccountFilter) (*Message, error) {
	log.Infof("Checking account Exist: %v:%v", ctx, info)
	exist, err := model.NewAccountDao().IsAccountExist(info.Id, info.Number, info.Type, info.Status)
	if err != nil {
		log.Errorf("Error in checking: %v", err)
		return &Message{
			Err:   "Not Found",
			Msg:   "Failed",
			Exist: exist,
			Valid: true,
		}, err
	} else if !exist {
		log.Errorf("Error in checking: %v", http.ErrNotSupported)
		return &Message{
			Err:   "Not Found",
			Msg:   "Failed",
			Exist: exist,
			Valid: true,
		}, http.ErrNotSupported
	}
	log.Debugf("Account exist: %v", exist)
	return &Message{
		Msg:   "Success",
		Exist: exist,
		Valid: true,
	}, nil
}

func (w WalletService) GetAccountBalance(ctx context.Context, filter *AccountFilter) (*BalanceInfo, error) {
	log.Infof("Get Account Balance: %v:%v", ctx, filter)
	balance, err := model.NewAccountDao().
		GetAccountBalance(filter.Id, filter.Type, filter.Status)
	if err != nil {
		log.Errorf("Error in checking: %v", err)
		return balance, err
	}
	return balance, nil
}

func (w WalletService) GetTransaction(ctx context.Context, filter *TransactionFilter) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, filter)
	//todo: need to calling step for transfer service
	return nil, http.ErrNotSupported
}

func (w WalletService) FindTransactions(filter *TransactionFilter, server WalletService_FindTransactionsServer) error {
	log.Infof("%v, %v", filter, server)
	//todo: need to calling step for search service
	return http.ErrNotSupported
}

func (w WalletService) InitiateTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, info)
	//todo: need to calling step for transfer service
	return nil, http.ErrNotSupported
}

func (w WalletService) ConfirmTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, info)
	//todo: need to calling step for transfer service
	return nil, http.ErrNotSupported
}

func (w WalletService) RevertTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, info)
	//todo: need to calling step for transfer service
	return nil, http.ErrNotSupported
}

func (w WalletService) RequestTransfer(ctx context.Context, info *TransactionInfo) (*Message, error) {
	log.Infof("%v:%v", ctx, info)
	//todo: need to calling step for transfer service
	return nil, http.ErrNotSupported
}

func (w WalletService) ResponseTransferRequest(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	log.Infof("%v:%v", ctx, info)
	//todo: need to calling step for transfer service
	return nil, http.ErrNotSupported
}

func (w WalletService) ManageAccount(ctx context.Context, info *AccountInfo) (*AccountInfo, error) {
	log.Infof("%v:%v", ctx, info)
	//todo: need to calling step for account service
	return nil, http.ErrNotSupported
}

func (w WalletService) mustEmbedUnimplementedWalletServiceServer() {
	panic("implement me")
}
