package main

import (
	"context"
	model "core-account/schema"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func Test_connectDb(t *testing.T) {
	db := pg.Connect(&pg.Options{
		User:     "nid2",
		Password: "nid2123",
		Database: "nid2",
		Addr:     "192.168.5.195:5432",
		OnConnect: func(ctx context.Context, cn *pg.Conn) error {
			_, err := cn.Exec("set search_path=?", "cws")
			if err != nil {
				return err
			}
			return nil
		},
	})
	fmt.Println("Db connection...", db.Conn())

	defer db.Close()
	count, err := db.Model(&model.Account{}).Count()
	if err != nil {
		panic(err)
	}
	fmt.Println("Counts", count)
	assert.Equal(t, http.StatusOK, 200)
}
