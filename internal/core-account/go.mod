module core-account

go 1.16

require (
	common v0.0.0-00010101000000-000000000000
	github.com/Shopify/sarama v1.29.1 // indirect
	//gopkg.in/confluentinc/confluent-kafka-go.v1 v1.7.0
	//github.com/confluentinc/confluent-kafka-go v1.7.0 // indirect
	github.com/go-pg/pg/v10 v10.10.5
	github.com/go-redis/redis/v8 v8.11.4 // indirect
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/golang/mock v1.6.0 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.1.2
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/labstack/echo-contrib v0.11.0
	github.com/labstack/echo/v4 v4.6.0
	github.com/lovoo/goka v1.0.6 // indirect
	github.com/pierrec/lz4 v2.6.1+incompatible // indirect
	github.com/stretchr/testify v1.7.0
	go.opentelemetry.io/otel v0.14.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20210928044308-7d9f5e0b762b // indirect
	google.golang.org/genproto v0.0.0-20210909211513-a8c4777a87af // indirect
	google.golang.org/grpc v1.40.0
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

replace common => ../../common
