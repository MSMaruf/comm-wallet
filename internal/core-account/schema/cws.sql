create schema cws;

-- Drop table

-- DROP TABLE cws.account;

CREATE TABLE cws.account
(
    acc_name        varchar   NOT NULL,
    acc_no          varchar   NOT NULL,
    initial_balance numeric   NOT NULL DEFAULT 0.00,
    last_balance    numeric   NOT NULL DEFAULT 0.00,
    acc_status      int8      NOT NULL DEFAULT 0,
    acc_type        int8      NOT NULL DEFAULT 0,
    created         timestamp NOT NULL DEFAULT now(),
    updated         timestamp NULL     DEFAULT now(),
    closed          timestamp NULL     DEFAULT now(),
    remarks         varchar   NULL,
    enc_pin         varchar   NOT NULL,
    hash_id         varchar   NOT NULL,
    id              serial    NOT NULL,
    CONSTRAINT account_pk PRIMARY KEY (id)
)
    WITH (
        OIDS= TRUE
    );
CREATE INDEX account_acc_no_idx ON cws.account USING btree (acc_no, acc_status, acc_type, hash_id, id);

-- Drop table

-- DROP TABLE cws.journal;

CREATE TABLE cws.journal
(
    id           serial    NOT NULL,
    id_hash      varchar   NOT NULL,
    acc_source   varchar   NOT NULL,
    acc_target   varchar   NOT NULL,
    prev_balance numeric   NOT NULL DEFAULT 0.00,
    next_balance numeric   NOT NULL DEFAULT 0.00,
    amount       numeric   NOT NULL DEFAULT 0.00,
    acc_src_id   int4      NOT NULL,
    acc_trg_id   int4      NOT NULL,
    uuid_trx     uuid      NOT NULL,
    created      timestamp NOT NULL DEFAULT now(),
    updated      timestamp NOT NULL DEFAULT now(),
    trx_status   int8      NOT NULL DEFAULT 0,
    trx_type     int8      NOT NULL DEFAULT 0,
    trx_channel  varchar   NOT NULL DEFAULT 'upay'::character varying,
    remarks      varchar   NOT NULL DEFAULT 'processing'::character varying,
    CONSTRAINT journal_pk PRIMARY KEY (id),
    CONSTRAINT journal_un UNIQUE (uuid_trx, id_hash, created, acc_src_id, acc_trg_id, acc_source, acc_target, trx_type)
)
    WITH (
        OIDS= TRUE
    );
CREATE INDEX journal_uuid_trx_idx ON cws.journal USING btree (uuid_trx, id, id_hash, acc_source, acc_target, acc_src_id,
                                                              acc_trg_id, created, trx_status, trx_type);

-- Drop table

-- DROP TABLE cws.ledger;

CREATE TABLE cws.ledger
(
    id          serial    NOT NULL,
    hash_id     varchar   NOT NULL,
    acc_no      varchar   NOT NULL,
    acc_id      int4      NOT NULL,
    trx_id      int4      NOT NULL,
    trx_hash_id varchar   NOT NULL,
    trx_uid     uuid      NOT NULL,
    amount      numeric   NOT NULL DEFAULT 0.00,
    balance     numeric   NOT NULL DEFAULT 0.00,
    created     timestamp NOT NULL DEFAULT now(),
    debit       int2      NOT NULL DEFAULT 0,
    trx_channel varchar   NOT NULL DEFAULT 'upay':: character varying,
    remarks     varchar   NOT NULL DEFAULT 'processed':: character varying,
    origin_id   int4      NOT NULL DEFAULT 0,
    origin_desc varchar   NOT NULL DEFAULT 'system':: character varying,
    trx_type    int8      NOT NULL DEFAULT 0,
    CONSTRAINT ledger_pk PRIMARY KEY (id),
    CONSTRAINT ledger_un UNIQUE (acc_no, acc_id, trx_id, trx_uid, trx_hash_id)
)
    WITH (
        OIDS = TRUE
    );
CREATE INDEX ledger_id_idx ON cws.ledger USING btree (id, hash_id, acc_no, acc_id, trx_id, trx_hash_id, trx_uid,
                                                      origin_id, debit);
