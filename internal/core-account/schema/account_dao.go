package model

import (
	. "common/proto"
	"context"
	. "core-account/config"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
	"github.com/prometheus/common/log"
	"net/http"
	"strconv"
)

type AccountDao struct {
	Db *pg.DB
}

func NewAccountDao() *AccountDao {
	return &AccountDao{}
}

func (ac *AccountDao) GetAccount(accountId string, name string,
	accountType AccountType, number string, status AccountStatus) (Account, error) {
	log.Infof("Get Account from DB:")
	ac.Db = ac.initiateConnection()
	defer ac.Db.Close()
	account := ac.createAccountCriteria(accountId, number, accountType, status, name)
	err := ac.Db.Model(&account).Select()
	if err != nil {
		return Account{}, err
	}
	return account, http.ErrNotSupported
}

func (ac *AccountDao) initiateConnection() *pg.DB {
	return pg.Connect(&pg.Options{
		User:     PgConfig.User,
		Password: PgConfig.Password,
		Database: PgConfig.Database,
		Addr:     PgConfig.Host,
		OnConnect: func(ctx context.Context, cn *pg.Conn) error {
			_, err := cn.Exec("set search_path=?", PgConfig.Schema)
			if err != nil {
				return err
			}
			return nil
		},
	})
}

func (ac *AccountDao) IsAccountExist(accountId string, number string, accountType AccountType,
	status AccountStatus) (bool, error) {
	log.Infof("Checking account exist in DB: %v-%v", accountId, number)
	ac.Db = ac.initiateConnection()
	defer ac.Db.Close()
	account := ac.createAccountCriteria(accountId, number, accountType, status, "")
	exists, err := ac.Db.Model(&account).Exists()
	if err != nil {
		return false, err
	}
	return exists, nil
}

func (ac *AccountDao) createAccountCriteria(accountId string, number string, accountType AccountType,
	status AccountStatus, name string) Account {
	account := Account{}
	if accountId != "" {
		id, _ := strconv.ParseInt(accountId, 0, 64)
		account.Id = id
	}
	if number != "" {
		account.AccNo = number
	}
	if accountType.String() != "" {
		account.AccType = int32(accountType)
	}
	if status.String() != "" {
		account.AccStatus = int32(status)
	}
	if name != "" {
		account.AccName = name
	}
	return account
}

func (ac *AccountDao) CreateNewAccount(info *AccountInfo) (*AccountInfo, error) {
	log.Infof("Creating on DB: %v", info)
	ac.Db = ac.initiateConnection()
	defer ac.Db.Close()
	account := ac.createAccountCriteria("", info.Number, info.Type, info.Status, info.Name)
	uid, _ := uuid.NewUUID()
	account.HashId = uid.URN()
	account.EncPin = info.EncPin
	initialBalance, _ := strconv.ParseFloat(info.Balance, 32)
	account.InitialBalance = float32(initialBalance)
	insert, err := ac.Db.Model(&account).Insert()
	if err != nil {
		return nil, err
	} else if insert.RowsAffected() == 0 {
		return nil, http.ErrNotSupported
	}
	log.Debugf("Created Account: %v", account)
	return &AccountInfo{
		Id:      fmt.Sprintf("%v", account.Id),
		Name:    account.AccName,
		Number:  account.AccNo,
		Type:    AccountType(account.AccType),
		Status:  AccountStatus(account.AccStatus),
		Balance: fmt.Sprintf("%v", account.InitialBalance),
	}, nil
}

func (ac *AccountDao) CloseAccount(info *AccountInfo) (*AccountInfo, error) {
	log.Infof("Closing Account:...%v", info)
	//todo: need to add db logic
	return nil, http.ErrNotSupported
}

func (ac *AccountDao) GetAccountBalance(accountId string, accountType AccountType,
	status AccountStatus) (*BalanceInfo, error) {
	log.Infof("Get Account Balance from last transaction: %v", accountId)
	account, err := ac.GetAccount(accountId, "", accountType, "", status)
	if err != nil {
		return &BalanceInfo{
			Id:      fmt.Sprintf("%v", accountId),
			Balance: fmt.Sprintf("%v", 0.0),
			Error:   fmt.Sprintf("%v", err),
			Msg:     "Failed to Get Any",
		}, err
	}
	log.Debugf("Initial Balance: %v", account.InitialBalance)
	transfer, err2 := ac.GetLastTransfer(accountId)
	if err2 != nil {
		return &BalanceInfo{
			Id:      fmt.Sprintf("%v", account.Id),
			Balance: fmt.Sprintf("%v", account.InitialBalance),
			Error:   fmt.Sprintf("%v", err2),
			Msg:     "Return Initial Balance",
		}, err2
	} else if transfer == nil {
		return &BalanceInfo{
			Id:      fmt.Sprintf("%v", account.Id),
			Balance: fmt.Sprintf("%v", account.InitialBalance),
			Error:   fmt.Sprintf("%v", http.ErrNotSupported),
			Msg:     "Return Initial Balance",
		}, http.ErrNotSupported
	}
	log.Debugf("Last transfer: %v", transfer)
	return &BalanceInfo{
		Id:           fmt.Sprintf("%v", account.Id),
		Balance:      fmt.Sprintf("%v", transfer.Balance),
		LastTransfer: fmt.Sprintf("%v", transfer),
		Msg:          "Fetch Last Transfer",
	}, nil

}

func (ac *AccountDao) GetLastTransfer(accountId string) (*Ledger, error) {
	log.Infof("Get From DB: %v", accountId)
	ac.Db = ac.initiateConnection()
	defer ac.Db.Close()
	ledger := Ledger{}
	if accountId != "" {
		id, _ := strconv.ParseInt(accountId, 0, 64)
		ledger.AccId = int32(id)
	}
	err := ac.Db.Model(&ledger).Order("id").Last()
	if err != nil {
		log.Errorf("Error in checking: %v", err)
		return nil, err
	}
	return &ledger, nil
}
