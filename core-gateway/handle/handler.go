package handle

import (
	. "common/proto"
	"context"
	. "core-gateway/proto"
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

func ManageAccount(c echo.Context) error {
	//todo: need to well defined management options
	c.Logger().Infof("Create account...")
	client := NewWalletClient()
	account, err := client.ManageAccount(
		context.Background(),
		&AccountInfo{Id: c.Param("id"),
			Name:    c.Param("name"),
			Number:  c.Param("number"),
			Balance: c.Param("balance"),
			Msg:     c.Param("reason"),
			Action:  AccountAction_Update})
	if err != nil {
		c.Logger().Errorf("Error occurred: %v", err)
		return err
	}
	defer client.Conn.Close()
	_ = c.JSON(http.StatusOK, account)
	return nil
}

func RevertTransfer(c echo.Context) error {
	c.Logger().Infof("Confirm Transfer...")
	client := NewWalletClient()
	debit, _ := strconv.ParseBool(c.Param("debit"))
	//todo: params should be as http.body
	transfer, err := client.ConfirmTransfer(context.Background(),
		&TransactionInfo{Id: c.Param("id"),
			Source:  c.Param("source"),
			Debit:   debit,
			Type:    TransactionType_Reverting,
			Pin:     c.Param("pin"),
			Msg:     c.Param("reason"),
			Target:  c.Param("target"),
			Balance: c.Param("balance"),
			Amount:  c.Param("amount")})
	if err != nil {
		return err
	}
	defer client.Conn.Close()
	_ = c.JSON(http.StatusOK, transfer)
	return nil
}

func RequestTransfer(c echo.Context) error {
	c.Logger().Infof("Request Transfer...")
	client := NewWalletClient()
	//todo: params should be as http.body
	transfer, err := client.RequestTransfer(context.Background(),
		&TransactionInfo{Source: c.Param("source"),
			Debit:   false,
			Msg:     "Requesting",
			Type:    TransactionType_Requesting,
			Target:  c.Param("target"),
			Balance: c.Param("balance"),
			Amount:  c.Param("amount")})
	if err != nil {
		return err
	}
	defer client.Conn.Close()
	_ = c.JSON(http.StatusOK, transfer)
	return nil
}

func ConfirmTransfer(c echo.Context) error {
	c.Logger().Infof("Confirm Transfer...")
	client := NewWalletClient()
	debit, _ := strconv.ParseBool(c.Param("debit"))
	//todo: params should be as http.body
	transfer, err := client.ConfirmTransfer(context.Background(),
		&TransactionInfo{Id: c.Param("id"),
			Source:  c.Param("source"),
			Debit:   debit,
			Target:  c.Param("target"),
			Balance: c.Param("balance"),
			Amount:  c.Param("amount")})
	if err != nil {
		return err
	}
	defer client.Conn.Close()
	_ = c.JSON(http.StatusOK, transfer)
	return nil
}

func InitiateTransfer(c echo.Context) error {
	c.Logger().Infof("Initiate Transfer...")
	client := NewWalletClient()
	debit, _ := strconv.ParseBool(c.Param("debit"))
	//todo: params should be as http.body
	transfer, err := client.InitiateTransfer(context.Background(),
		&TransactionInfo{Source: c.Param("source"),
			Debit:   debit,
			Pin:     c.Param("pin"),
			Target:  c.Param("target"),
			Balance: c.Param("balance"),
			Amount:  c.Param("amount")})
	if err != nil {
		return err
	}
	defer client.Conn.Close()
	_ = c.JSON(http.StatusOK, transfer)
	return nil
}

func GetAccountBalance(c echo.Context) error {
	c.Logger().Infof("Get Account Balance...")
	client := NewWalletClient()
	balance, err := client.GetAccountBalance(context.Background(),
		&AccountFilter{Id: c.Param("id"),
			Name: c.Param("name")})
	if err != nil {
		c.Logger().Errorf("Error occurred: %v", err)
		return err
	}
	defer client.Conn.Close()
	_ = c.JSON(http.StatusOK, balance)
	return nil
}

func CreateAccount(c echo.Context) error {
	c.Logger().Infof("Create account...")
	client := NewWalletClient()
	account, err := client.CreateAccount(
		context.Background(),
		&AccountInfo{Name: c.Param("name"),
			Number:  c.Param("number"),
			Balance: c.Param("balance"),
			Action:  AccountAction_Create})
	if err != nil {
		c.Logger().Errorf("Error occurred: %v", err)
		return err
	}
	defer client.Conn.Close()
	_ = c.JSON(http.StatusOK, account)
	return nil
}

func GetAccount(c echo.Context) error {
	c.Logger().Infof("Get account...")
	client := NewWalletClient()
	account, err := client.GetAccount(
		context.Background(),
		&AccountFilter{Id: c.Param("id")})
	if err != nil {
		c.Logger().Errorf("Error occurred: %v", err)
		return err
	}
	defer client.Conn.Close()
	_ = c.JSON(http.StatusOK, account)
	return nil
}

func CheckAccount(c echo.Context) error {
	c.Logger().Infof("Checking account...")
	fmt.Println("Checking account...")
	client := NewWalletClient()
	fmt.Println("Client of service:..", client.Conn.GetState())
	message, err := client.CheckAccount(
		context.Background(),
		&AccountFilter{Id: c.Param("id")})
	if err != nil {
		fmt.Println(err)
		c.Logger().Errorf("Error occurred: %v", err)
		return err
	}
	defer client.Conn.Close()
	_ = c.JSON(http.StatusOK, message)
	return nil
}
