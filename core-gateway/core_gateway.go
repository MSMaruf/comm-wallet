package main

import (
	"context"
	. "core-gateway/config"
	. "core-gateway/handle"
	"fmt"
	"github.com/labstack/echo-contrib/jaegertracing"
	"github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
	_ "github.com/swaggo/echo-swagger/example/docs"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

// @title Swagger Wallet API
// @version 1.0
// @description This is a sample server wallet gateway.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host com.tiger.it.wallet need actual host
// @BasePath / need actual context path
func main() {
	log.Printf("Starting Core-Gateway:....")
	e := echo.New()
	initManage(e)
	initServer(e)
}

func initServer(e *echo.Echo) {
	c := jaegertracing.New(e, UrlSkipper)
	addUrlMapping(e)
	// Start server
	go func() {
		port := os.Getenv("PORT")
		if port == "" {
			port = HostConfig.Port
		}
		if err := e.Start(fmt.Sprintf(":%s", port)); err != nil && err != http.ErrServerClosed {
			e.Logger.Fatal("shutting down the server")
		}
	}()
	http.Handle("/", e)
	graceFullShutdown(e, c)
}

func addUrlMapping(e *echo.Echo) {
	e.GET("/", rootPath)
	e.GET("/swagger/*", echoSwagger.WrapHandler)
	e.GET("/check-account", CheckAccount)
	e.GET("/get-account", GetAccount)
	e.POST("/creat-account", CreateAccount)
	e.POST("/get-account-balance", GetAccountBalance)
	e.POST("/initial-transfer", InitiateTransfer)
	e.POST("/confirm-transfer", ConfirmTransfer)
	e.POST("/revert-transfer", RevertTransfer)
	e.POST("/request-transfer", RequestTransfer)
	e.POST("/manage-account", ManageAccount)
}

func graceFullShutdown(e *echo.Echo, c io.Closer) {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_ = c.Close()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}

// rootPath godoc
// @Summary Show gateway running
// @Description Show gateway running
// @Accept  json
// @Produce  json
// @Success 200 {object} String
// @Header 200 {string} Token "auth-token"
// @Failure 400,404 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Failure default {object} httputil.DefaultError
// @Router / [get]
func rootPath(c echo.Context) error {
	return c.String(http.StatusOK, "Hello! This is gateway for core wallet")
}

func initManage(e *echo.Echo) {
	p := prometheus.NewPrometheus("echo", UrlSkipper)
	p.Use(e)
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(CORSConfig))
	e.Use(middleware.CSRFWithConfig(CSRFConfig))
	//e.Use(middleware.JWTWithConfig(JWTConfig))
	e.Use(middleware.TimeoutWithConfig(TimeoutConfig))
}
