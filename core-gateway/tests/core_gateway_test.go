package tests

import (
	. "core-gateway/handle"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_account_check(t *testing.T) {
	fmt.Printf("Check account access calling: (%v)...", t.Name())
	rec, c := Setup("/check-account", "100001", http.MethodGet)
	// Assertions
	if assert.NoError(t, CheckAccount(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
	}
}

func Test_account_create(t *testing.T) {
	fmt.Printf("Check account access calling: (%v)...", t.Name())
	rec, c := Setup("/create-account", "{number: 1000004}", http.MethodPost)
	// Assertions
	if assert.NoError(t, CreateAccount(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
	}
}

func Test_account_get(t *testing.T) {
	fmt.Printf("Check account access calling: (%v)...", t.Name())
	rec, c := Setup("/get-account", "{number: 1000004}", http.MethodPost)
	// Assertions
	if assert.NoError(t, CreateAccount(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
	}
}

func Test_initiate_transfer(t *testing.T) {
	fmt.Printf("Check initate transfer calling: (%v)...", t.Name())
	rec, c := Setup("/initiate-transfer",
		"{source: 100004, target: 100005, balance: 5000, type: 0, amount: 1000, previous: 4000}",
		http.MethodPost)

	if assert.NoError(t, InitiateTransfer(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
	}
}

func Test_confirm_transfer(t *testing.T) {
	fmt.Printf("Check initate transfer calling: (%v)...", t.Name())
	rec, c := Setup("/confirm-transfer",
		"{source: 100004, target: 100005, balance: 5000, type: 0, amount: 1000, previous: 4000}",
		http.MethodPost)

	if assert.NoError(t, InitiateTransfer(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
	}
}

func Test_account_search(t *testing.T) {
	fmt.Printf("Check account create calling: (%v)...", t.Name())
	assert.Equal(t, http.StatusNotFound, http.StatusNotFound)
}

func Setup(url string, params string, httpMethod string) (*httptest.ResponseRecorder, echo.Context) {
	e := echo.New()
	req := httptest.NewRequest(httpMethod, url, strings.NewReader(params))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	return rec, c
}
