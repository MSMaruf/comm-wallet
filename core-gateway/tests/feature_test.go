package tests

import (
	"fmt"
	"testing"
)

func Test_GatewayUrl(t *testing.T) {
	fmt.Printf("Start testing gateway calling to service: %v \n\r", t.Name())
	fmt.Println("Account creation...")
	fmt.Println("Account balance check...")
	fmt.Println("Account closing...")
	fmt.Println("Account settlement...")
	fmt.Println("Account manage...")
	fmt.Println("Account recover...")
	fmt.Println("Initiate transfer...")
	fmt.Println("Confirm transfer...")
	fmt.Println("Revert transfer...")
	fmt.Println("Request transfer...")
	fmt.Println("Search transfer...")
	fmt.Println("Search history...")
}
