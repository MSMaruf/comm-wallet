package config

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func DefaultSkipper(ctx echo.Context) bool {
	return false
}

var (
	CORSConfig = middleware.CORSConfig{
		Skipper:      DefaultSkipper,
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut,
			http.MethodPatch, http.MethodPost, http.MethodDelete},
	}
	CSRFConfig = middleware.CSRFConfig{
		Skipper:      DefaultSkipper,
		TokenLength:  32,
		TokenLookup:  "header:" + echo.HeaderXCSRFToken,
		ContextKey:   "csrf",
		CookieName:   "_csrf",
		CookieMaxAge: 86400,
	}

	JWTConfig     = middleware.DefaultJWTConfig
	TimeoutConfig = middleware.TimeoutConfig{
		Timeout: 5 * time.Second,
	}
	HostConfig = NewHostAddressConfig().Init()
)

func UrlSkipper(c echo.Context) bool {
	if strings.HasPrefix(c.Path(), "/test-url") {
		return true
	}
	return false
}

type HostAddressConfig struct {
	Port                string `yaml:"port"`
	RemoteServiceHost   string `yaml:"remote_service_host"`
	RemoteAccountHost   string `yaml:"remote_account_host"`
	RemoteTransferHost  string `yaml:"remote_transfer_host"`
	RemoteSearchHost    string `yaml:"remote_search_host"`
	RemoteMessagingHost string `yaml:"remote_messaging_host"`

	RedisHosts    string `yaml:"redis_hosts"`
	RedisPassword string `yaml:"redis_password"`
	RedisUser     string `yaml:"redis_user"`

	KafkaHosts               string `yaml:"kafka_hosts"`
	KafkaTopicAuditTail      string `yaml:"kafka_topic_audit_tail"`
	KafkaGroupAuditTail      string `yaml:"kafka_group_audit_tail"`
	KafkaTopicAccountStatus  string `yaml:"kafka_topic_account_status"`
	KafkaGroupAccountStatus  string `yaml:"kafka_group_account_status"`
	KafkaTopicTransferStatus string `yaml:"kafka_topic_transfer_status"`
	KafkaGroupTransferStatus string `yaml:"kafka_group_transfer_status"`
	KafkaTopicNotifyStatus   string `yaml:"kafka_topic_notify_status"`
	KafkaGroupNotifyStatus   string `yaml:"kafka_group_notify_status"`
	kafkaTopicMsgStatus      string `yaml:"kafka_topic_msg_status"`
	kafkaGroupMsgStatus      string `yaml:"kafka_group_msg_status"`
}

func (c *HostAddressConfig) Init() *HostAddressConfig {
	yamlFile, err := ioutil.ReadFile("app.yaml")
	if err != nil {
		fmt.Printf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		fmt.Printf("Unmarshal: %v", err)
	}
	return c
}
func NewHostAddressConfig() *HostAddressConfig {
	return &HostAddressConfig{}
}
