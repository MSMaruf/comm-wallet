package proto

import (
	. "common/proto"
	"context"
	. "core-gateway/config"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
)

type WalletClient struct {
	Client WalletServiceClient
	Conn   *grpc.ClientConn
}

func (w WalletClient) GetAccount(ctx context.Context, in *AccountFilter, _ ...grpc.CallOption) (*AccountInfo, error) {
	return w.Client.GetAccount(ctx, in)
}

func (w WalletClient) CreateAccount(ctx context.Context, in *AccountInfo, _ ...grpc.CallOption) (*AccountInfo, error) {
	return w.Client.CreateAccount(ctx, in)
}

func (w WalletClient) CloseAccount(ctx context.Context, in *AccountInfo, _ ...grpc.CallOption) (*AccountInfo, error) {
	return w.Client.CloseAccount(ctx, in)
}

func (w WalletClient) CheckAccount(ctx context.Context, in *AccountFilter, _ ...grpc.CallOption) (*Message, error) {
	return w.Client.CheckAccount(ctx, in)
}

func (w WalletClient) GetAccountBalance(ctx context.Context, in *AccountFilter, _ ...grpc.CallOption) (*BalanceInfo, error) {
	return w.Client.GetAccountBalance(ctx, in)
}

func (w WalletClient) GetTransaction(ctx context.Context, in *TransactionFilter, _ ...grpc.CallOption) (*TransactionInfo, error) {
	return w.Client.GetTransaction(ctx, in)
}

func (w WalletClient) FindTransactions(ctx context.Context, in *TransactionFilter, _ ...grpc.CallOption) (WalletService_FindTransactionsClient, error) {
	return w.Client.FindTransactions(ctx, in)
}

func (w WalletClient) InitiateTransfer(ctx context.Context, in *TransactionInfo, _ ...grpc.CallOption) (*TransactionInfo, error) {
	return w.Client.InitiateTransfer(ctx, in)
}

func (w WalletClient) ConfirmTransfer(ctx context.Context, in *TransactionInfo, _ ...grpc.CallOption) (*TransactionInfo, error) {
	return w.Client.ConfirmTransfer(ctx, in)
}

func (w WalletClient) RevertTransfer(ctx context.Context, in *TransactionInfo, _ ...grpc.CallOption) (*TransactionInfo, error) {
	return w.Client.RevertTransfer(ctx, in)
}

func (w WalletClient) RequestTransfer(ctx context.Context, in *TransactionInfo, _ ...grpc.CallOption) (*Message, error) {
	return w.Client.RequestTransfer(ctx, in)
}

func (w WalletClient) ResponseTransferRequest(ctx context.Context, in *TransactionInfo, _ ...grpc.CallOption) (*TransactionInfo, error) {
	return w.Client.ResponseTransferRequest(ctx, in)
}

func (w WalletClient) ManageAccount(ctx context.Context, in *AccountInfo, _ ...grpc.CallOption) (*AccountInfo, error) {
	return w.Client.ManageAccount(ctx, in)
}

func NewWalletClient() *WalletClient {
	fmt.Println("Established connection")
	conn, err := grpc.DialContext(context.Background(),
		HostConfig.RemoteServiceHost,
		grpc.WithReturnConnectionError(),
		grpc.WithBlock(),
		grpc.WithNoProxy(),
		grpc.WithInsecure())
	if err != nil || conn.GetState() == connectivity.Idle {
		fmt.Println("Failed to connect ", err)
		panic(fmt.Sprintf("Failed to connect %v", err))
	}
	fmt.Println("Get new Client")
	client := NewWalletServiceClient(conn)
	fmt.Println(client, err)
	return &WalletClient{Client: client, Conn: conn}
}
