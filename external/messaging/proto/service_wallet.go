package proto

import (
	. "common/proto"
	"context"
	"net/http"
)

type WalletService struct {
}

type CoreService struct{}

func (c CoreService) VerifyPin(ctx context.Context, info *TransactionInfo) (*Message, error) {
	panic("Should Not Implemented")
}

func (c CoreService) VerifyBalance(ctx context.Context, info *TransactionInfo) (*Message, error) {
	panic("Should Not Implemented")
}

func (c CoreService) VerifyAccounts(ctx context.Context, info *TransactionInfo) (*Message, error) {
	panic("Should Not Implemented")
}

func (c CoreService) VerifyAccount(ctx context.Context, info *AccountInfo) (*Message, error) {
	panic("Should Not Implemented")
}

func (c CoreService) LockAccount(ctx context.Context, info *TransactionInfo) (*Message, error) {
	panic("Should Not Implemented")
}

func (c CoreService) UnlockAccount(ctx context.Context, info *TransactionInfo) (*Message, error) {
	panic("Should Not Implemented")
}

func (c CoreService) NotifyTransfer(ctx context.Context, info *TransactionInfo) (*Message, error) {
	return nil, http.ErrNotSupported
}

func (c CoreService) NotifyAccount(ctx context.Context, info *AccountInfo) (*Message, error) {
	return nil, http.ErrNotSupported
}

func (c CoreService) PushAuditTrail(ctx context.Context, message *Message) (*Message, error) {
	return nil, http.ErrNotSupported
}

func (c CoreService) PushAccountAction(ctx context.Context, info *AccountInfo) (*Message, error) {
	return nil, http.ErrNotSupported
}

func (c CoreService) PushTransaction(ctx context.Context, info *TransactionInfo) (*Message, error) {
	return nil, http.ErrNotSupported
}

func (c CoreService) PushNotification(ctx context.Context, notification *Notification) (*Message, error) {
	return nil, http.ErrNotSupported
}

func (c CoreService) mustEmbedUnimplementedCoreServiceServer() {
	panic("implement me")
}

func NewCoreService() *CoreService {
	return &CoreService{}
}

func NewWalletService() *WalletService {
	return &WalletService{}
}

func (w WalletService) GetAccount(ctx context.Context, filter *AccountFilter) (*AccountInfo, error) {
	return nil, http.ErrNotSupported
}
func (w WalletService) CreateAccount(ctx context.Context, filter *AccountInfo) (*AccountInfo, error) {
	return nil, http.ErrNotSupported
}
func (w WalletService) CloseAccount(ctx context.Context, filter *AccountInfo) (*AccountInfo, error) {
	return nil, http.ErrNotSupported
}

func (w WalletService) CheckAccount(ctx context.Context, filter *AccountFilter) (*Message, error) {
	return nil, http.ErrNotSupported
}

func (w WalletService) GetAccountBalance(ctx context.Context, filter *AccountFilter) (*BalanceInfo, error) {
	return nil, http.ErrNotSupported
}

func (w WalletService) GetTransaction(ctx context.Context, filter *TransactionFilter) (*TransactionInfo, error) {
	return nil, http.ErrNotSupported
}

func (w WalletService) FindTransactions(filter *TransactionFilter, server interface{}) error {
	return http.ErrNotSupported
}

func (w WalletService) InitiateTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	return nil, http.ErrNotSupported
}

func (w WalletService) ConfirmTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	return nil, http.ErrNotSupported
}

func (w WalletService) RevertTransfer(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	return nil, http.ErrNotSupported
}

func (w WalletService) RequestTransfer(ctx context.Context, info *TransactionInfo) (*Message, error) {
	return nil, http.ErrNotSupported
}

func (w WalletService) ResponseTransferRequest(ctx context.Context, info *TransactionInfo) (*TransactionInfo, error) {
	return nil, http.ErrNotSupported
}

func (w WalletService) ManageAccount(ctx context.Context, info *AccountInfo) (*AccountInfo, error) {
	return nil, http.ErrNotSupported
}

func (w WalletService) mustEmbedUnimplementedWalletServiceServer() {
	panic("implement me")
}
