module consumer

go 1.16

require (
	common v0.0.0-00010101000000-000000000000
	gopkg.in/yaml.v2 v2.4.0
)

replace common => ../../common
